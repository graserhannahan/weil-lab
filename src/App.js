import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home.js";
import Navigation from "./components/Navigation.js";

function App() {
  return (
    <>
    <Navigation />
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    </>
  );
}

export default App;
